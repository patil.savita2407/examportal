from django.contrib import admin

from .models import Stream
from .models import Subject
from .models import Question_bank,Question,Complexity

admin.site.register(Stream)
admin.site.register(Subject)
admin.site.register(Question_bank)
admin.site.register(Question)
admin.site.register(Complexity)