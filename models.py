from django.db import models


class Stream(models.Model):
    stream_id = models.CharField(max_length=200)
    stream_name = models.CharField(max_length=200)
  
    pub_date = models.DateTimeField('date published')


class Subject(models.Model):
   subject_name = models.CharField(max_length=200)
   subject_id = models.CharField(max_length=200)
   stream_id  = models.ForeignKey(Stream, on_delete=models.CASCADE)
   # votes = models.IntegerField(default=0)
     